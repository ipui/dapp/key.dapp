# key.dapp

single purpose dapp to manage your ipfs keys.

> **this is not an implementation of an entire ipfs node**, this project only use the `api` that the _daemon_ binds over the port `5001` (by default). [go](https://github.com/ipfs/go-ipfs) or [javascript](https://github.com/ipfs/js-ipfs) implementation is by your own.

## ip[fn]s links
* [/ipns/QmepRQWsyeJ5TQphYV39y4JChAYpQjABhJQu7bNqYCB2rM](https://ipfs.io/ipns/QmepRQWsyeJ5TQphYV39y4JChAYpQjABhJQu7bNqYCB2rM) (permanent)
* [key.dapp.rabrux.space](http://key.dapp.rabrux.space) (permanent)
* [/ipfs/QmQnP9MvUnCz1JTmUdvxbXDAVEVR6mADsktYNBRk6DrtuR](https://ipfs.io/ipfs/QmQnP9MvUnCz1JTmUdvxbXDAVEVR6mADsktYNBRk6DrtuR) latest

## technologies

* [reactjs](https://reactjs.org)
* [ipfs](https://ipfs.io)

## dev

> This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
