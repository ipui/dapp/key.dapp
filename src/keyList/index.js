import React from 'react';
import { withKey } from '../key';
import KeyActionMenu from '../keyActionMenu';

// icons
import {
  FiMoreVertical
} from 'react-icons/fi';

class KeyList extends React.Component {
  state = {
    selected: null
  }

  constructor( props ) {
    super( props );
    this.select = this.select.bind( this );
  }

  select( item ) {
    this.setState( ( prevState, props ) => {
      return {
        ...prevState,
        selected: item
      };
    } );
  }

  render() {
    const { list } = this.props.withKey;
    const { selected } = this.state

    const itemList = list.map( el => {
      return (
        <li key={ el.id }>
          <label>{ el.name }</label>
          <button onClick={ () => this.select( el ) }>
            <FiMoreVertical />
          </button>
        </li>
      );
    } )

    return (
      <main className="column">
        <ul>
          { itemList }
        </ul>
        { selected ? (
          <KeyActionMenu
            item={ selected }
            closeWith={ () => this.select( null ) }
          />
        ) : null }
      </main>
    )
  }
}

export default withKey( KeyList );
