import React from 'react';
import Ipfs from './ipfs';
import Key from './key';
import NoIpfs from './noIpfs';
import TopBar from './topBar';
import KeyList from './keyList';
import BottomBar from './bottomBar';
import { homepage, bugs } from '../package.json';

function App() {
  return (
    <Ipfs noIpfs={ NoIpfs } links={ { homepage, issues: bugs.url } } >
      <Key>
        <TopBar />
        <KeyList />
        <BottomBar />
      </Key>
    </Ipfs>
  );
}

export default App;
