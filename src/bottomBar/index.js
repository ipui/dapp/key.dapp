import React, { useState } from 'react';
import KeyGen from '../keyGen';

import {
  FiPlus
} from 'react-icons/fi';

function BottomBar( props ) {
  const [ bar, setBar ] = useState( null );

  return (
    <>
      { !bar ? (
        <footer className="right">
          <button onClick={ e => setBar( 'keyGen' ) }>
            <FiPlus />
          </button>
        </footer>
      ) : null }

      { bar === 'keyGen' ? ( <KeyGen closeIt={ () => setBar( null ) } /> ) : null }
    </>
  );
}

export default BottomBar;
