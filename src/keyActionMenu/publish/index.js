import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withKey } from '../../key';
import Modal from '../../modal';
import Loader from '../../loader';

import { IoMdGlobe } from 'react-icons/io';
import { FiCheck, FiX } from 'react-icons/fi';

function Publish( props ) {
  const [ showForm, setShowForm ] = useState( false );
  const [ isLoading, setIsLoading ] = useState( false );
  const [ cid, setCid ] = useState( '' );

  const { item, close } = props;

  function action() {
    const { name: key } = item;
    const { api } = props.withKey;

    setIsLoading( true );

    api
      .publish( cid, { key } )
      .then( detail => {
        close();
      } )
      .catch( console.error )
  }

  function toggle() {
    setShowForm( !showForm );
  }

  return (
    <>
      <li onClick={ toggle }>
        <figure>
          <IoMdGlobe />
        </figure>
        <label>publish</label>
      </li>

      { showForm ? (
        <Modal closeWith={ toggle }>
          <header>
            <h1>{ item.name }</h1>
            <button onClick={ toggle }>
              <FiX />
            </button>
          </header>

          <main>
            <div className="row">
              <input
                type="text"
                placeholder="cid"
                size="1"
                value={ cid }
                onChange={ e => setCid( e.target.value ) }
                autoFocus
              />
            </div>
          </main>

          <footer>
            <button className="full" onClick={ action }>
              <FiCheck />
              <label>publish</label>
            </button>
          </footer>
        </Modal>
      ) : null }

      { isLoading ? (
        <Loader>
          <p>publishing...</p>
        </Loader>
      ) : null }
    </>
  );
}

Publish.propTypes = {
  item: PropTypes.object.isRequired,
  close: PropTypes.func
}

export default withKey( Publish );
