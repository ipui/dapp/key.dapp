import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withKey } from '../../key';
import Modal from '../../modal';

import { FiEdit2, FiCheck, FiX } from 'react-icons/fi';

function Rename( props ) {
  const [ showForm, setShowForm ] = useState( false );
  const [ newName, setNewName ] = useState( '' );

  const { item, close } = props;

  function rename() {
    const { name: oldName } = item;
    const { api, refresh } = props.withKey;

    api
      .rename( oldName, newName )
      .then( key => {
        refresh();
        close();
      } )
      .catch( console.error )
  }

  function toggle() {
    setShowForm( !showForm );
  }

  return (
    <>
      <li onClick={ toggle }>
        <figure>
          <FiEdit2 />
        </figure>
        <label>rename</label>
      </li>

      { showForm ? (
        <Modal closeWith={ toggle }>
          <header>
            <h1>{ item.name }</h1>
            <button onClick={ toggle }>
              <FiX />
            </button>
          </header>

          <main>
            <div className="row">
              <input
                type="text"
                placeholder="new name"
                size="1"
                value={ newName }
                onChange={ e => setNewName( e.target.value ) }
                autoFocus
              />
            </div>
          </main>

          <footer>
            <button className="full" onClick={ rename }>
              <FiCheck />
              <label>rename</label>
            </button>
          </footer>
        </Modal>
      ) : null }
    </>
  );
}

Rename.propTypes = {
  item: PropTypes.object.isRequired,
  close: PropTypes.func
}

export default withKey( Rename );
