import React from 'react';
import PropTypes from 'prop-types';
import copyToClipboard from '../../lib/copyToClipboard';

import { FiShare2 } from 'react-icons/fi';

function Share( props ) {
  const { item, close } = props;

  function share() {
    copyToClipboard( 'https://ipfs.io/ipns/' + item.id );
    close();
  }

  return (
    <li onClick={ share }>
      <figure>
        <FiShare2 />
      </figure>
      <label>share</label>
    </li>
  );
}

Share.propTypes = {
  item: PropTypes.object.isRequired,
  close: PropTypes.func
}

export default Share;
