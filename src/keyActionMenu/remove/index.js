import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withKey } from '../../key';

import {
  FiTrash,
  FiAlertOctagon
} from 'react-icons/fi';

class Remove extends Component {
  state = {
    showConfirm: false
  };

  timeout = undefined;

  static propTypes = {
    item: PropTypes.object.isRequired,
    close: PropTypes.func,
    delay: PropTypes.number
  }

  constructor( props ) {
    super( props );
    this.action = this.action.bind( this );
    this.toggle = this.toggle.bind( this );
    this.waitConfirm = this.waitConfirm.bind( this );
  }

  componentWillUnmount() {
    clearTimeout( this.timeout );
  }

  action() {
    const { item, close } = this.props
    const { api, refresh } = this.props.withKey;

    api
      .rm( item.name )
      .then( key => {
        refresh();
        close();
      } )
      .catch( console.error )
  }

  waitConfirm() {
    const { delay = 2000 } = this.props;

    this.toggle();

    this.timeout = setTimeout( () => {
      this.toggle();
    }, delay );
  }

  toggle() {
    this.setState( ( prevState, props ) => {
      return {
        ...prevState,
        showConfirm: !prevState.showConfirm
      };
    } );
  }

  render() {
    const { showConfirm } = this.state;

    return (
      <>
        { showConfirm ? (
          <li onClick={ this.action }>
            <figure>
              <FiAlertOctagon />
            </figure>
            <label>click to confirm</label>
          </li>
        ) : (
          <li onClick={ this.waitConfirm }>
            <figure>
              <FiTrash />
            </figure>
            <label>remove</label>
          </li>
        ) }
      </>
    );
  }
}

export default withKey( Remove );
