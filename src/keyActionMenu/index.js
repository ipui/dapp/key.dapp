import React from 'react';
import Menu from '../menu';

// actions
import CopyCid from './copyCid';
import Share from './share';
import Publish from './publish';
import Rename from './rename';
import Remove from './remove';

// icons
import { FiX } from 'react-icons/fi';

function KeyActionMenu( props ) {
  const { item, closeWith } = props;

  return (
    <Menu closeWith={ closeWith }>
      <header>
        <h1>{ item.name }</h1>
        <button onClick={ () => closeWith() }>
          <FiX />
        </button>
      </header>
      <ul>
        <CopyCid item={ item } close={ closeWith } />

        <Share item={ item } close={ closeWith } />

        <Publish item={ item } close={ closeWith } />

        { item.name !== 'self' ? (
          <>
            <Rename item={ item } close={ closeWith } />

            <Remove item={ item } close={ closeWith } />

          </>
        ) : null }
      </ul>
    </Menu>
  );
}

export default KeyActionMenu;
