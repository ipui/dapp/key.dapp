import React, { useState } from 'react';
import IpfsConnect from '../ipfsConnect';

// icons
import {
  FiSettings,
  FiX
} from 'react-icons/fi';

function TopBar( props ) {
  const [ isToggled, setToggled ] = useState( false );

  function toggle() {
    setToggled( !isToggled );
  }

  return (
    <>
      <header>
        <h1>keychain</h1>
        <button onClick={ toggle }>
          { isToggled ? ( <FiX /> ) : ( <FiSettings /> ) }
        </button>
      </header>
      { isToggled ? (
        <header>
          <IpfsConnect />
        </header>
      ) : null }
    </>
  );
}

export default TopBar;
