import React, { Component } from 'react';
import { withKey } from '../key';
import InlineForm, { InlineFormStep } from '../inlineForm';

import {
  FiArrowRight,
  FiCheck,
  FiX
} from 'react-icons/fi';

class KeyGen extends Component {

  state = {
    name: '',
    type: '',
    size: '2048'
  }

  constructor( props ) {
    super( props );
    this.keyGen = this.keyGen.bind( this );
    this.updateModel = this.updateModel.bind( this );
    this.reset = this.reset.bind( this );
  }

  keyGen() {
    const { api, refresh } = this.props.withKey;
    const { closeIt } = this.props;

    const { name, ...options } = this.state;

    api
      .gen( name, options )
      .then( key => {
        refresh();
        closeIt();
      } )
      .catch( console.error )
  }

  reset() {
    const { closeIt } = this.props;
    closeIt();
  }

  updateModel( key, value ) {
    this.setState( ( prevState, props ) => {
      return {
        ...prevState,
        [ key ]: value
      };
    } )
  }

  render() {
    const { name, type, size } = this.state;

    return (
      <InlineForm onSubmit={ this.keyGen } onReset={ this.reset }>
        <InlineFormStep>
          <footer>
            <input
              type="text"
              placeholder="key name"
              size="1"
              value={ name }
              onChange={ e => this.updateModel( 'name', e.target.value ) }
              autoFocus
              required
            />
      
            <button type="submit">
              <FiArrowRight />
            </button>
      
            <button type="reset">
              <FiX />
            </button>
          </footer>
        </InlineFormStep>
      
        <InlineFormStep>
          <footer>
            <select
              value={ type }
              onChange={ e => this.updateModel( 'type', e.target.value ) }
              autoFocus
              required
            >
              <option disabled value="">type</option>
              <option>rsa</option>
              <option>ed25519</option>
            </select>
      
            { type === 'rsa' ? (
              <select
                value={ size }
                onChange={ e => this.updateModel( 'size', e.target.value ) }
              >
                <option disabled value="">size</option>
                <option>2048</option>
                <option>4096</option>
              </select>
            ) : null }
      
            <button type="submit">
              <FiCheck />
            </button>
      
            <button type="reset">
              <FiX />
            </button>
          </footer>
        </InlineFormStep>
      </InlineForm>
    );
  }
}

export default withKey( KeyGen );
