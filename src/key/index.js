import React, { Component, createContext } from 'react';
import PropTypes from 'prop-types';
import { withIpfs } from '../ipfs';

const KeyContext = createContext();

class Key extends Component {
  state = {
    list: []
  }

  static propTypes = {
    children: PropTypes.node.isRequired
  }

  constructor( props ) {
    super( props );
    this.refresh = this.refresh.bind( this );
  }

  componentDidMount() {
    this.refresh();
  }

  refresh() {
    const { node: api } = this.props.withIpfs;
    api
      .key.list()
      .then( list => {
        this.setState( ( prevState, props ) => {
          return {
            ...prevState,
            list
          }
        } )
      } )
      .catch( console.error )
  }

  render() {
    const { list } = this.state
    const { refresh } = this

    return (
      <KeyContext.Provider value={ {
        list,
        refresh,
        api: {
          ...this.props.withIpfs.node.key,
          publish: this.props.withIpfs.node.name.publish
        }
      } }>
        { this.props.children }
      </KeyContext.Provider>
    )
  }

}

function withKey( ComponentAlias ) {
  return props => (
    <KeyContext.Consumer>
      { context => {
        return <ComponentAlias { ...props } withKey={ context } />
      } }
    </KeyContext.Consumer>
  );
}

export default withIpfs( Key );

export {
  withKey
}
